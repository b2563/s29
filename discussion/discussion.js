// [SECTION] Query Operators
	// allows us to expand our queries and define conditions instead of looking for specific values

//[Section] Comparison Query Operators

// $gt/$gte operator
/*
	- Allows us to find documents that have field number values greater than or equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $gt : value } });
		db.collectionName.find({ field : { $gte : value } });
*/

// $gt - greater than
db.users.find({ age : { $gt : 65 } });

// $gte - greater than or equal to
db.users.find({ age : { $gte : 65 } });

// $lt/$lte operator
/*
	- Allows us to find documents that have field number values less than or equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $lt : value } });
		db.collectionName.find({ field : { $lte : value } });
*/

// $lt - less than
db.users.find({ age : { $lt : 65 } });

// $lte - less than or equal to
db.users.find({ age : { $lte : 65 } });

// $ne operator
/*
	- Allows us to find documents that have field number values not equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $ne : value } });
*/
db.users.find({ age : { $ne: 82 } });

// [Section] Evaluation Query Operators

// $regex operator
/*
	- Allows us to find documents that match a specific string pattern using regular expressions.
	- Syntax
		db.users.find({ field: $regex: 'pattern', $options: '$optionValue' });
*/

// Case sensitive query
db.users.find({ firstName: { $regex: 'N' } });

// Case insensitive query
db.users.find({ firstName: { $regex: 'N', $options: '$i' } });

// [Section] Logical Query Operators

// $or operator
/*
	- Allows us to find documents that match a single criteria from multiple provided search criteria.
	- Syntax
		db.collectionName.find({ $or: [ { fieldA: valueB }, { fieldB: valueB } ] });
*/
db.users.find({ $or: [{firstName: "Neil"}, {age: 21}]});

db.users.find({ $or: [{firstName: "Neil"}, {age: { $gt: 30}}]});

// $and operator
/*
	- Allows us to find documents matching multiple criteria in a single field.
	- Syntax
		db.collectionName.find({ $and: [ { fieldA: valueB }, { fieldB: valueB } ] });
*/
db.users.find({$and: [{age: {$gte: 65}}, {age: {$lte: 90}}]});

// [Section] Field Projection
/*
	- Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response.
	- When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
	- To help with readability of the values returned, we can include/exclude fields from the response.
*/

// Inclusion
/*
	- Allows us to include/add specific fields only when retrieving documents.
	- The value provided is 1 to denote that the field is being included.
	- Syntax
		db.users.find({criteria},{field: 1})
*/
db.users.find({ firstName: "Jane"},{firstName: 1, lastName: 1, age: 1});

// Exclusion
/*
	- Allows us to exclude/remove specific fields only when retrieving documents.
	- The value provided is 0 to denote that the field is being included.
	- Syntax
		db.users.find({criteria},{field: 0})
*/
db.users.find({ firstName: "Jane"},{lastName: 0, age: 0, _id: 0});

// Suppressing the ID field
/*
	- Allows us to exclude the "_id" field when retrieving documents.
	- When using field projection, field inclusion and exclusion may not be used at the same time.
	- Excluding the "_id" field is the only exception to this rule.
	- Syntax
		db.users.find({criteria},{_id: 0})
*/
db.users.find({ firstName: "Jane"},{firstName: 1, lastName: 1, age: 1, _id:0});

// Returning Specific Fields in Embedded Documents
db.users.find({ firstName: "Jane"},{firstName: 1, lastName: 1, age: 1, _id:0, "contact.phone": 1});

// Supressing Specific Fields in embedded documents
db.users.find({ firstName: "Jane"},{"contact.phone": 0});
